import React, { useEffect, useMemo, useRef, useState } from "react";

export interface IActiveIntersectionRef {
	register: (ref: HTMLElement) => void;
}

const calculateHowFarFromMiddle = ({
	boundingClientRect: { bottom, top },
	rootBounds,
}: IntersectionObserverEntry): number =>
	Math.abs(top - (rootBounds!.bottom - bottom));

const findMiddleIntersectingElement = (
	intersectingEntries: IntersectionObserverEntry[]
): IntersectionObserverEntry | undefined => {
	console.log(
		intersectingEntries.map(en => ({
			elem: en.target,
			calc: calculateHowFarFromMiddle(en),
			bt: en.boundingClientRect.bottom,
			pb: en.rootBounds?.bottom,
			top: en.boundingClientRect.top,
			pt: en.rootBounds?.top,
		}))
	);
	return intersectingEntries.sort(
		(a, b) => calculateHowFarFromMiddle(a) - calculateHowFarFromMiddle(b)
	)[0];
};

const useAcriveIntersection = (
	intersectionRef: React.MutableRefObject<IActiveIntersectionRef | null>
) => {
	const [currentActiveId, setCurrentActiveId] = useState<string>("");
	const intersectingEntries = useRef<{
		[key: string]: IntersectionObserverEntry;
	}>({});
	const intersectionObserver = useMemo(
		() =>
			new IntersectionObserver(
				entries => {
					entries.forEach(entry => {
						if (entry.isIntersecting) {
							return (intersectingEntries.current[entry.target.id] = entry);
						}
						Reflect.deleteProperty(
							intersectingEntries.current,
							entry.target.id
						);
					});
					console.log({
						intersectingEntries: Object.keys(intersectingEntries.current),
						mid: findMiddleIntersectingElement(
							Object.values(intersectingEntries.current)
						),
					});
					const middleIntersectingElement = findMiddleIntersectingElement(
						Object.values(intersectingEntries.current)
					);
					if (
						middleIntersectingElement &&
						middleIntersectingElement.target.id !== currentActiveId
					) {
						setCurrentActiveId(middleIntersectingElement.target.id);
					}
				},
				{
					threshold: [0, 0.25, 0.5, 0.75, 1],
				}
			),
		[]
	);
	useEffect(() => {
		intersectionRef.current = {
			register(element) {
				intersectionObserver.observe(element);
			},
		};
	}, []);
	return {
		currentActiveId,
	};
};
export default useAcriveIntersection;
