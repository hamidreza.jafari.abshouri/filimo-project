import { getMostViewedVideos } from "Store/reducers/mostViewedVideosSlice";
import { store } from "Store/store";

export const loadRedux = () => {
	store.dispatch(getMostViewedVideos());
};
