import { loadRedux } from "./reduxLoader";

const loaders = {
	init: () => {
		loadRedux();
	},
};

export default loaders;
