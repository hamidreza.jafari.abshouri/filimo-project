import styled from "styled-components";
import { useSelector } from "react-redux";
import { selectVideos } from "Store/reducers/mostViewedVideosSlice";
import Video from "Components/Video";
import { useRef } from "react";
import useAcriveIntersection, {
	IActiveIntersectionRef,
} from "Hooks/useAcriveIntersection";
import { spaceYMixinFactory } from "Styles/mixins/SpaceMixins";

const Home: React.FC = () => {
	const videos = useSelector(selectVideos);
	const intersectionRef = useRef<IActiveIntersectionRef | null>(null);
	const { currentActiveId } = useAcriveIntersection(intersectionRef);
	return (
		<Container>
			{videos.map(video => (
				<Video
					video={video}
					isActive={video.id === currentActiveId}
					intersectionRef={intersectionRef}
				/>
			))}
		</Container>
	);
};

export default Home;

const Container = styled.div`
	display: flex;
	flex-direction: column;
	padding: 1em;
	${spaceYMixinFactory("large")}
`;
