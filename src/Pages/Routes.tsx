import { Route, Switch } from "react-router-dom";
import Home from "./Home";

const Routes: React.FC = () => {
	return (
		<Switch>
			<Route path="/" component={Home} />
		</Switch>
	);
};

export default Routes;
