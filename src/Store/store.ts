import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import mostViewedVideosSlice from "./reducers/mostViewedVideosSlice";

export const store = configureStore({
	reducer: {
		mostViewedVideos: mostViewedVideosSlice,
	},
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
	ReturnType,
	RootState,
	unknown,
	Action<string>
>;
