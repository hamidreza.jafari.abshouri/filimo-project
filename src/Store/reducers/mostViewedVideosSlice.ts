import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { api } from "Api/Api";
import { Video } from "Api/GeneratedApi";
import { RootState } from "../store";

interface MostViewedVideosState {
	videos: Video[];
	status: "idle" | "loading" | "failed";
}

const initialState: MostViewedVideosState = {
	videos: [],
	status: "idle",
};

export const getMostViewedVideos = createAsyncThunk(
	"mostViewedVideos/getMostViewedVideos",
	async () => {
		const response = await api.getMostViewedVideos();
		return response.data.data;
	}
);

export const mostViewedVideosSlice = createSlice({
	name: "mostViewedVideos",
	initialState,
	reducers: {},
	extraReducers: builder => {
		builder
			.addCase(getMostViewedVideos.pending, state => {
				state.status = "loading";
			})
			.addCase(getMostViewedVideos.fulfilled, (state, action) => {
				state.status = "idle";
				state.videos = action.payload;
			});
	},
});

export const selectVideos = (state: RootState) => state.mostViewedVideos.videos;

export default mostViewedVideosSlice.reducer;
