// styled.d.ts
import "styled-components";

declare module "styled-components" {
	export interface DefaultTheme {
		zIndex: {
			high: number;
			higher: number;
			highest: number;
		};
		palette: {
			common: {
				black: string;
				white: string;
				dark: strnig;
				darker: string;
				light: string;
				lighter: strnig;
				backgorund: string;
			};
			primary: PaletteType;
			secondary: PaletteType;
			tertiary: PaletteType;
		};
	}
}

type PaletteType = {
	main: string;
	contrast: string;
	shade: string;
	tint: string;
};
