import { css, keyframes } from "styled-components";

const blinkAnimation = keyframes`
    0% {
        opacity: 0;
    }

    25% {
        opacity: 1;
    }

    75% {
        opacity: 1;
    }

    100% {
        opacity: 0;
    }
`;

export const blinkAnimationMixin = css`
	animation: ${blinkAnimation} 2s linear infinite;
`;
