import { css } from "styled-components";

export const spaceXMixinFactory = (size: "small" | "large") => css`
	& > * + * {
		margin-right: ${size === "large" ? "1em" : "0.5em"};
	}
`;
export const spaceYMixinFactory = (size: "small" | "large") => css`
	& > * + * {
		margin-top: ${size === "large" ? "1em" : "0.5em"};
	}
`;
