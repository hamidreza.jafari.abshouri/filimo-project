import styled from "styled-components";

const Layout: React.FC = ({ children }) => {
	return (
		<Container>
			<Header>پربازدید ترین ویدئو ها</Header>
			<ContentContainer>
				<Page>{children}</Page>
			</ContentContainer>
			<Footer>تمامی حقوق محفوظ است</Footer>
		</Container>
	);
};

export default Layout;

const Container = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	background-color: ${p => p.theme.palette.common.backgorund};
	overflow: hidden;
`;
const Header = styled.header`
	width: 100%;
	display: flex;
	justify-content: center;
	position: relative;
	overflow: hidden;
	padding: 1em;
	background-color: ${p => p.theme.palette.secondary.main};
	color: ${p => p.theme.palette.secondary.contrast};
`;
const ContentContainer = styled.main`
	width: 100%;
	display: inline-block;
	position: relative;
	flex: 1;
	overflow: hidden;
`;
const Page = styled.main`
	height: 100%;
	max-height: 100%;
	overflow-y: scroll;
`;
const Footer = styled.footer`
	display: flex;
	justify-content: center;
	width: 100%;
	padding: 0.5em;
	background-color: ${p => p.theme.palette.secondary.main};
	color: ${p => p.theme.palette.secondary.contrast};
`;
