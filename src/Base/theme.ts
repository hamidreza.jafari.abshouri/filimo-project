import { DefaultTheme } from "styled-components";

const theme: DefaultTheme = {
	zIndex: {
		high: 1,
		higher: 10,
		highest: 100,
	},
	palette: {
		primary: {
			main: "#dccd7c",
			contrast: "#000128",
			shade: "#a99c56",
			tint: "#ecde90",
		},
		secondary: {
			main: "#3da1b8",
			contrast: "#fff",
			shade: "#00476d",
			tint: "#44ceec",
		},
		tertiary: {
			main: "#a8df5d",
			contrast: "#000000",
			shade: "#94c452",
			tint: "#b1e26d",
		},
		common: {
			black: "#000",
			white: "#fff",
			dark: "#555",
			darker: "#333",
			light: "#bbb",
			lighter: "#ddd",
			backgorund: "#f9f9f9",
		},
	},
};

export default theme;
