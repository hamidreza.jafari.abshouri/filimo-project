import { Video as VideoType } from "Api/GeneratedApi";
import { IActiveIntersectionRef } from "Hooks/useAcriveIntersection";
import React, { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { blinkAnimationMixin } from "Styles/mixins/AnimationMixins";
import {
	spaceXMixinFactory,
	spaceYMixinFactory,
} from "Styles/mixins/SpaceMixins";
import { formatSeconds } from "Utils/timeUtils";

type Props = {
	video: VideoType;
	isActive: boolean;
	intersectionRef: React.MutableRefObject<IActiveIntersectionRef | null>;
};

const Video: React.FC<Props> = ({ video, isActive, intersectionRef }) => {
	const videoRef = useRef<HTMLVideoElement>(null);
	const [isLoading, setIsLoading] = useState(false);
	useEffect(() => {
		intersectionRef.current!.register(videoRef.current!);
	}, []);
	useEffect(() => {
		videoRef.current!.onloadstart = _ => setIsLoading(true);
		videoRef.current!.onloadeddata = _ => setIsLoading(false);
		videoRef.current!.onloadedmetadata = e => console.log({ e });

		if (isActive) {
			videoRef.current!.load();
			setTimeout(() => {
				videoRef.current!.play();
			}, 200);
		} else {
			videoRef.current!.pause();
		}
	}, [isActive]);
	return (
		<Container>
			<InformationContainer>
				<ProfilePhoto src={video.attributes.profilePhoto} />
				<ProfileInfos>
					<h4>{video.attributes.sender_name}</h4>
				</ProfileInfos>
			</InformationContainer>
			<VideoContainer>
				<VideoTag
					id={video.id}
					crossOrigin="anonymous"
					autoPlay
					playsInline
					muted
					preload="metadata"
					ref={videoRef}
					src={isActive ? video.attributes.preview_src : ""}
				/>
				{!isActive && <Poster src={video.attributes.big_poster} />}
				{isActive && isLoading && (
					<DurationViewer>
						{formatSeconds(+video.attributes.duration)}
					</DurationViewer>
				)}
			</VideoContainer>
		</Container>
	);
};

export default Video;

const Container = styled.div`
	width: 100%;
	position: relative;
	padding: 1em;
	border-radius: 0.5em;
	overflow: hidden;
	padding: 0.5em;
	box-shadow: 0px 0px 21px -2px rgba(0, 0, 0, 0.22);
	background-color: ${p => p.theme.palette.common.white};
	display: flex;
	flex-direction: column;
	${spaceYMixinFactory("large")}
`;

const InformationContainer = styled.div`
	display: flex;
	align-items: center;
	${spaceXMixinFactory("small")}
`;
const ProfilePhoto = styled.img`
	border-radius: 50%;
	width: 3em;
	height: 3em;
	object-fit: cover;
`;
const ProfileInfos = styled.div`
	display: flex;
	flex-direction: column;
	flex: 1;
`;

const VideoContainer = styled.div`
	width: 100%;
	padding-top: 60%;
	overflow: hidden;
	position: relative;
`;

const VideoTag = styled.video`
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0%;
	left: 0;
	border-radius: 0.5em;
	background-color: black;
	width: 100%;
	height: 100%;
	object-fit: contain;
	overflow: hidden;
`;

const Poster = styled.div<{ src: string }>`
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0%;
	left: 0;
	background-image: url(${p => p.src});
	background-size: cover;
	background-position: center;
	border-radius: 0.5em;
	overflow: hidden;
`;

const DurationViewer = styled.p`
	position: absolute;
	margin: 0;
	top: 1em;
	left: 1em;
	color: ${p => p.theme.palette.common.white};
	${blinkAnimationMixin};
`;
