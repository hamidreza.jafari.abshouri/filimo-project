import Routes from "Pages/Routes";
import Layout from "Base/Layout";

function App() {
	return (
		<Layout>
			<Routes />
		</Layout>
	);
}

export default App;
