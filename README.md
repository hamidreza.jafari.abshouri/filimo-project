# filimo test project

This project was created for filimo interview test project

for running project do the followings

1. clone the project
2. run `npm install` for installing dependencies
3. run `npm run buildapi` for generating api
4. run `npm start` and open [http://localhost:3000](http://localhost:3000) to view it in the browser.
